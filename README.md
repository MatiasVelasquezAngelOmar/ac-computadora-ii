# Computadoras Electronicas

```plantuml
@startmindmap

*[#lightcoral] Computadoras Electronicas
**[#SlateBlue] Se necesito Inovar para abaratar costos\n y espacio de las antiguas computadoras
**[#SlateBlue] Las computadoras electromecanicas tendian a fallar mucho\n por el uso de relés
***[#Silver] Los relés se desgastaban mucho \ny se les metian insectos(bugs)
**[#SlateBlue] Fue posible gracias a:
***[#RosyBrown] La Valvula termoionica
****[#Silver] Formada por un filamento y dos electrodos situados dentro de un vuvlo
*****[#Silver]_ El filamento se Calienta
******[#Silver] Permite el flujo de electrones desde el Anodo hacia el Catodo
****[#Silver] Se mejoro con electrodo de control
*****_ Permite el flujo o detiene la corriente
******[#Silver] Asiendo la misma funcion que un relé
****[#Silver] Puede cambiar miles de veces por segundo
****[#Silver] Fueran la base para:
*****[#Silver] *Radio\n*Telefonos\n*Muchos disposotivos electronicos
****[#Silver] Las computadoras requerian cientos de miles
****[#Silver] Dio inicio a las computadoras electronicas
****[#Silver] Su primer gra uso fue en la Colossus MARK 1
*****[#Silver] Diseñada por el ingeniero Tommy Flowers
*****[#Silver] Se finalizo en Diciembre de 1943
*****[#Silver] Se uso para decodificar las comuniaciones NAZIS
*****[#Silver] Tenia 1600 tubos de Vacio
*****[#Silver] Se construyeron 10 Colossus
*****[#Silver] Se le conocio como la primer computadora electronica programable
*****[#Silver] Se configuraba mediante la coneccion de cientos de cables
****[#Tan] Luego de Colossus Vino la ENIAC
*****[#Tan] Siglas de Calcuadora Integadora Numerica Electronica
*****[#Tan] Se construyo en 1946
*****[#Tan] En la Universidad de Pensinvalnia
*****[#Tan] Fue diseñada por Jhon Mauchly y J.Presper Ecker
*****[#Tan] Fue la primera Computadora Electronica Programable de Proposito General
*****[#Tan] Podia realizar 5 mil sumas y restas de 10 digitos por segundo
*****[#Tan] Opero durante diez años
*****[#Tan] Se calcula que realizo mas operaciones aritmeticas que toda la Humanidad hasta ese momento
*****[#Tan] Devido a las constantes fallas solo funcionada durante medio dia hasta dañarse
***[#RosyBrown] Los Transistores
****_ En 1947
*****[#Turquoise] Cientificos de los laboratorios BELL
******[#Turquoise] *Jhon Barden\n*Walter Brattain\n*William Shockley
******_ Crearon el Transistor
****[#Turquoise] Cumple la misma funcion que un relé o un tubo de vacio
****[#Turquoise] Estan hechos de silicio
****[#Turquoise] Gracias al Doping
*****[#Turquoise] Puede conducir corriente electrica
*****[#Turquoise] Dependdiendo del elemento que se agergue
******[#Turquoise] Puede tener exeso o defecto de electrones
****[#Turquoise] Esta construridos con tres capas de tipos de Silicio
*****[#Turquoise] Las tres capas  son:
******[#Turquoise] *Colector\n*Emisor\n*Base
****[#Turquoise] Puede cambiar de estado 10 mil veces por segundo
****[#Turquoise] Eran muy resistentes 
****[#Turquoise] Podian hacerce mucho mas pequeñas que un relé o un tubo de vacio
*****_ Gracias a eso se construllo la IBM 608
******[#Turquoise] En 1957
****[#Turquoise] La primer computadora comercial 
*****[#Turquoise] Podia realizar cerca de 4500 Sumas por segundo
*****[#Turquoise] 80 Diviciones o multiplicaiones por segundo
****[#Turquoise] Hoy en dia los transistores tiene un tamaño menor a 50nm
*****[#Turquoise] Pueden cambiar de estado millones de veces por segundo
*****[#Turquoise] Funcionan por decadas






@endmindmap

```

# Arquitectura Von Neumann y Arquitectura Harvard

```plantuml
@startmindmap

*[#lightcoral] Arquitectura Von Neuman y Arquitectura Hardvard
**[#lightblue] Arquitectura Von-Neuman
***[#Turquoise] Tres partes principales
****[#LightSalmon] CPU
*****[#LightSalmon] Unidad de Control
*****[#LightSalmon] Unidad Artitmetica Logica Y Registros
****[#LightSalmon] Memoria principal
*****[#LightSalmon] Almacena tanto datos como instrucciones
*****[#LightSalmon] Es segementada
****[#LightSalmon] Sistemas de I/O
***_ Tiene 
****[#LightPink] Registro de contador de programa
****[#LightPink] Registro de instrucicones
****[#LightPink] Registro de direccion de memoria
****[#LightPink] Registro de  buffer de Memoria
****[#LightPink] Registro de direcciones de  I/O
****[#LightPink] Registro de buffer de I/O
****[#LightPink] Todos eso interconectado por Buses
*****[#LightPink] Bus de de control
*****[#LightPink] Bus de Direciones
*****[#LightPink] Bus de Datos e Instrucciones
******[#LightPink] Es bidireccional
*****[#LightPink] Bus
******_ Es:
*******[#LightPink] Dispositivo comun entre dos o mas Dispositivos
******[#LightPink] Si dos Elementos transmiten al mismo bus ocaciona distorcion
*******_ El arbitraje decide quien usa el bus para evitar problemas 
******[#LightPink] Bus de Sistema
*******_ Interconecta las partes de la computadora
******[#LightPink] Su proposito es reducir la cantidad de conecciones\n de la CPU a los demas elementos
***[#Turquoise] Fue Descrita por Jhon Von Neuman
****[#LightSkyBlue] Modelo
*****[#LightSkyBlue] Los datos y programas se almacenan \n en una misma memoria de lectura
*****[#LightSkyBlue] Se accede al contenido indicando su posicion\nsin importar su tipo
*****[#LightSkyBlue] Ejecucion en Secuencia (Amenos que se indique lo contrario)
*****[#LightSkyBlue] Representacion Binaria
****[#LightSkyBlue] Programa almacenado
****[#LightSkyBlue] Tiene un cuello de sistema
*****[#LightSkyBlue] Por que los buses son usados por todos los dispositivos
*****[#LightSkyBlue] Cada Elemento tiene una velocidad diferente
***[#Turquoise] Ciclo de EJecucion
****[#LightGray] 1.-La unidad de control optiene una instruccion de la memoria
****[#LightGray] 2.-Usando el Registro de contador de programa
****[#LightGray] 3.-Dejando La informacion en el registro IR (Registro de instrucciones)
****[#LightGray] 4.-Se incrementa el Registro de Contador de Programa
****[#LightGray] 5.-La instruccion se decodifica del IR a un lenguaje que entienda la ALU
****[#LightGray] 6.-Obtiene de Memoria los operando requeridos por la instruccion
*****[#LightGray] *Fetch de Operandos\n*Calcular Operandos
****[#LightGray] 7.-La ALU ejecuta y deja los resultados en los registros o en memoria
*****[#LightGray] *Execute Instruccion(EI)\n*Write Operand (WO)
****[#LightGray] 8.-Volver al paso Uno
***[#Turquoise] Ciclo de Instruccion
****[#LightSkyBlue] 1.-Calculo la direccion de la instruccion
****[#LightSkyBlue] 2.-Captacion de la Instruccion
****[#LightSkyBlue] 3.-Decodificacion de la instruccion
****[#LightSkyBlue] 4.-Calculo de la direccion del operando
****[#LightSkyBlue] 5.-Captacion del Operando
****[#LightSkyBlue] 6.-Operacion de Datos
****[#LightSkyBlue] 7.-Calculo de la direccion del operando
****[#LightSkyBlue] 8.-Almacenamineto del Operando
****[#LightSkyBlue] Todo esto se raliza de manera iterativa
***[#Turquoise] Esta en todas las computadoras Modernas
**[#lightblue] Arquitectura Hardvard
***[#LightCyan] Usada Originalmente en computadoras con dispositivos de\n almacenamiento separdos fisicamente una para\n los datos y otra para las instrucicones
***[#LightCyan] Contiene
****[#LightCyan] CPU
*****[#LightCyan] Unidad de Control
******[#LightCyan] Lee las instrucciones de la memoria de instrucciones
******[#LightCyan] Al final ejecuta las instruccione mediante la ALU    
*****[#LightCyan] Unidad Aritmetica Logica y Registros
****[#LightCyan] Memoria Principal
*****[#LightCyan] Las instrucciones se almacenan en una memoria muy rapida\n pero pequeña llamada Cache
*****[#LightCyan] Memoria de Instrucciones
******[#LightCyan] Almacena las instrucciones del programa que ejecuta el microcontrolador
******[#LightCyan] Se implementa usando memorias  no volatiles
****[#LightCyan] Memoria de Datos 
*****[#LightCyan] Almacena los datos utilizados por los programas
*****[#LightCyan] Utiliza una memoria SRAM(Static RAM)
*****[#LightCyan] Sobre esa memoria realiza operaciones de lectura y escritura
****[#LightCyan] Sistema de I/O
***[#LightCyan] EL Modelo Hardvard soluciona el problema del cuello de botella \n implemnetado varias memorias cache para las instrucciones y los datos
***[#LightCyan] Esta arquitecura se usa en :
****[#LightCyan] PCIs o Microcontroladoes
****[#LightCyan] Productos de proposito especifico como:
*****[#LightCyan] *Electrodomesticos\n*Telecomunicaciones\n*Automoviles\n*Mouse\n*Impresoras\n*Procesamiento de Audio y Video , etc
***[#LightCyan] Hay buses Separados por cada tipo de memoria
****[#LightCyan] Agilizando la busqueda de datos y las instrucciones


@endmindmap
````
# Basura Electrónica

```plantuml
@startmindmap

*[#lightcoral] Basura Electronica
**_ Es:
***[#Azure] Dispositivos Electronicos que han llego al fin de su vida util
**[#BurlyWood] Se suele reciclar para:
***[#LightGreen] Crear una fuente de Trabajo
***[#LightGreen] Reducir costos de nuevos dispositivos
***_ Extraer
****[#LightGreen] *Oro\n*Plata\n*Cobre\n*Cobalto\n*Estaño\n*Silicio\n*Hierro\n*Bronce
**[#BurlyWood] La gran parte no es reciclada
***[#Azure] Una gran cantidad es exportada hacia:
****[#Azure] *China\n*India\n*Mexico
***[#Azure] Es desambalada para extraer\nmetales preciosos
**[#BurlyWood] Hay una mercado ilegal para deshacerse de ella
***[#Chocolate] Cuesta Demasiado Reciclarlos correctamente
***[#Chocolate] Se recicla de manera insegura en dichos paises
***[#Chocolate] Pude causar muchas enfermedades
**[#BurlyWood] Se buscan los dispositivos con datos
***_ Se usan para:
****[#Khaki] Con fines criminales
****[#Khaki] Los usan para robar identidad
***[#Khaki] Para prevenirlo se tratan de destruir los discos duros con imanes
**[#BurlyWood] Aumentan por que:
***[#LightCyan] El gran aumento de nuevos consuminadores
***[#LightCyan] Los dispositivos estan diseñados para durar poco
**[#BurlyWood] Son un problema Ambiental
**[#BurlyWood] Se les denomina como RAEE
***[#LightSalmon] Son Residuos o Aparatos Electricos y Electronicos
****_ Como
*****[#LightSalmon] *Licuadora\n*TV\n*Computadora\n*Celular
**[#BurlyWood] Se Selecciona segun su categoria
***[#Orange] Plasticos Y Metales
***[#Orange] Para despues ser desarmados
****[#Orange] Se segrega material peligroso
*****[#Orange] Tienen un trato diferente
****_ Recuperar materiales primarios
*****[#Orange] Metales
*****[#Orange] Plasticos
*****[#Orange] Cristales
***[#Orange] Para ser usados en la elaboracion de nuevos productos
****[#Orange] Enviados a otros paises
**[#BurlyWood] Pila boton
***[#Pink] Contamina hasta 600 l de agua
**[#BurlyWood] Al recilcar el aluminio
***[#Pink] Se emiten 0.29 Toneladas de CO2
**[#BurlyWood] Al usar Aluminio no reciclado
***[#Pink] Se emiten 3800 Toneladas de CO2
**[#BurlyWood] Deben dar un correcto tratamiento a los residuos:
***[#Thistle] El estado 
***[#Thistle] Fabricantes
***[#Thistle] Importadores
***[#Thistle] Esambladores
***[#Thistle] Comerciantes
**[#BurlyWood] El 70% de las toxinas de los tiraderos\n proviene de la basura electronica
***[#Yellow] Desprende toxinas en:
****[#Yellow] *Tierra\n*Aire\n*Agua
**[#BurlyWood] En México
***[#Turquoise] Se desechan cerca de 150 y 180 mil toneladas
****[#Turquoise] Dispositivos como:
*****[#Turquoise] *Televisores\n*Telefonos\n*Celulares
****[#Turquoise] Tienen una vida promedio de 3 años
**[#BurlyWood] El material reciclado se usa en:
***[#Teal] Suela de zapatos
***[#Teal] *Rines\n*Adornos\n*Adoquin\n*Ovalin de Baño
**[#BurlyWood] Hay empresar que logran reciclar hasta el 90% de los materiales
**[#BurlyWood] Recicladores Clandestinos
***[#Snow] Agarran lo que las grandes recicladoras no quieren
****_ Como
*****[#Snow] *Memorias RAMs\n*Mouses\n*Laptops\n*Baterias\n*Discos Duros\n*Audifonos\n*Impresoras\n*Cargadores\n*Consolas\n*Camaras\n*Reguladores\n*Proyectores\n*Telefonos\n*Electrodomesticos
***_ Que hacen
****[#Snow] Revender
****[#Snow] Reparar 
****[#Snow] Vender Repuestos



@endmindmap
```



